angular.module('todoApp', [])
.controller('ejemploController', ['$scope', '$http',
function($scope, $http) {


$scope.activo = function(id){
  $(".nav-item").removeClass("active");
  $("#"+id).addClass("active");
}


  $scope.casa = true;
  $http({
    method: 'GET',
    url: 'Json/heroes.json'
  }).then(function successCallback(response) {
    $scope.listheroes = response.data;
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });

  $scope.detailheroe = function(nombre,bio,aparicion,casa,img){
    $scope.heroenombre = nombre;
    $scope.heroebio = bio;
    $scope.heroeimg = img;
    if (casa == "dc") {
      $scope.casa = false;
    }else {
      $scope.casa = true;
    }
    $("#modal-detalle-heroe").modal("show");
  }


}]);
